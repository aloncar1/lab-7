package com.company;



import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.*;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.HashMap;


public class Main {

    public static void main(String[] args) {
        //ZADATAK1

        List<Integer> zadatak1 = new ArrayList<Integer>();
        zadatak1.add(1);
        zadatak1.add(2);
        zadatak1.add(3);
        zadatak1.add(4);
        zadatak1.add(5);
        System.out.println("Prije: " + zadatak1);
        //Remove 3rd item:
        zadatak1.remove(2);
        System.out.println("Nakon: " + zadatak1);


        //ZADATAK2
        /*
        List<Integer> zadatak2 = new ArrayList<Integer>();
        zadatak2.add(1);
        zadatak2.add(2);
        zadatak2.add(3);
        zadatak2.add(4);
        zadatak2.add(5);
        System.out.println("Prije: " + zadatak2);
         //Reverse arayList:
        Collections.reverse(zadatak2);
        System.out.println("Nakon: " + zadatak2);
        }
        */

        //ZADATAK3
        /*
        HashSet<String> zadatak3 = new HashSet<String>();
        zadatak3.add("Ovo");
        zadatak3.add("su");
        zadatak3.add("random");
        zadatak3.add("5");
        zadatak3.add("elemenata");
         //Show hash size:
        System.out.println("The size of the set is: " + zadatak3.size());
        */

        //ZADATAK4
        /*
        HashSet<String> zadatak4 = new HashSet<String>();
        zadatak4.add("Ovo");
        zadatak4.add("su");
        zadatak4.add("novih");
        zadatak4.add("5");
        zadatak4.add("stringova");
        System.out.println("Prije: " + zadatak4);
        //clear hasSet:
        zadatak4.clear();
        System.out.println("Nakon: " + zadatak4);
        */

        //ZADATAK5:
        /*
        TreeSet<Integer> zadatak5tree1 = new TreeSet<Integer>();
        zadatak5tree1.add(1);
        zadatak5tree1.add(2);
        zadatak5tree1.add(3);
        zadatak5tree1.add(4);
        zadatak5tree1.add(5);
        TreeSet<Integer> zadatak5tree2 = new TreeSet<Integer>();
        zadatak5tree2.add(6);
        zadatak5tree2.add(7);
        zadatak5tree2.add(8);
        zadatak5tree2.add(9);
        zadatak5tree2.add(10);
        System.out.println("Prije prvi: " + zadatak5tree1);
        System.out.println("Prije drugi: " + zadatak5tree2);
        zadatak5tree2.addAll(zadatak5tree1);
        System.out.println("Nakon prvi: " + zadatak5tree1);
        System.out.println("Nakon drugi: " + zadatak5tree2);
         */

        //ZADATAK6
        /*
        HashMap<String, String> zadatak6 = new HashMap<String, String>();
        zadatak6.put("England", "London");
        zadatak6.put("Germany", "Berlin");
        zadatak6.put("Norway", "Oslo");
        zadatak6.put("USA", "Washington DC");
        zadatak6.put("Croatia", "Zagreb");
        System.out.println(zadatak6.entrySet());
         */

        //ZADATAK7
        /*
        HashMap<String, String> zadatak7 = new HashMap<String, String>();
        zadatak7.put("England", "London");
        zadatak7.put("Germany", "Berlin");
        zadatak7.put("Norway", "Oslo");
        zadatak7.put("USA", "Washington DC");
        zadatak7.put("Croatia", "Zagreb");
        System.out.println("Ima li hrvatske? " + zadatak7.containsKey("Croatia"));
         */

        //ZADATAK8
        /*
        HashMap<String, String> zadatak8 = new HashMap<String, String>();
        zadatak8.put("England", "London");
        zadatak8.put("Germany", "Berlin");
        zadatak8.put("Norway", "Oslo");
        zadatak8.put("USA", "Washington DC");
        zadatak8.put("Croatia", "Zagreb");
        System.out.println("Value:" + zadatak8.get("Croatia"));
         */

        //ZADATAK9
        /*
        HashMap<String, String> zadatak9 = new HashMap<String, String>();
        zadatak9.put("England", "London");
        zadatak9.put("Germany", "Berlin");
        zadatak9.put("Norway", "Oslo");
        zadatak9.put("USA", "Washington DC");
        zadatak9.put("Croatia", "Zagreb");
        for (String key : zadatak9.values()) {
            System.out.println(key);
        }
        */
        //ZADATAK10
        TreeMap<Integer, String> zadatak10 = new TreeMap<Integer, String>();

        // Mapping string values to int keys
        zadatak10.put(1, "a");
        zadatak10.put(2, "b");
        zadatak10.put(3, "c");
        zadatak10.put(4, "d");
        zadatak10.put(5, "e");
        System.out.println("TreeMap prije: " + zadatak10);
        zadatak10.clear();
        System.out.println("TreeMap nakon: " + zadatak10);
    }
}
